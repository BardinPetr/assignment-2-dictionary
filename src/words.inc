%include "dict/colon.inc"

segment .data

dictionary_begin main_dict
    %assign i 0
    %rep 10

        %strcat key     "key_", %str(i)
        %strcat label   key, "_label"
        %strcat data    key, "_data"
        %deftok label   label

        colon key, label
        db data, 0

    %assign i i+1
    %endrep
dictionary_end
