%include "assignment-1-io-library/lib.inc"
%include "dict/dict.inc"
%include "src/words.inc"

%define BUFFER_SIZE 256

segment .data
message_error_overflow:      db "Input too long", 0
message_error_not_found:     db "Key not found", 0

segment .text

global _start:
_start:
    enter BUFFER_SIZE, 0

    lea rdi, [rbp-BUFFER_SIZE]
    mov rsi, BUFFER_SIZE
    call read_line

    test rax, rax
    jz .error_read


    lea rdi, [rbp-BUFFER_SIZE]
    mov rsi, [main_dict]
    call dict_find_word

    test rax, rax
    jz .not_found


    mov rdi, rax
    call dict_get_value
    mov rdi, rax
    call print_line

    xor rdi, rdi ; ret code
    jmp .end


.not_found:
    mov rdi, message_error_not_found
    call print_error
    mov rdi, 1
    jmp .end

.error_read:
    mov rdi, message_error_overflow
    call print_error
    mov rdi, 2

.end:
    leave
    jmp exit
