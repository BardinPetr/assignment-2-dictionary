PYTHON = python3
ASM = nasm -felf64 -g
LD = ld

OUT = build
TARGETS = $(OUT)/main.o $(OUT)/dict.o


.phony: all run test clean

all: $(OUT) $(TARGETS) $(OUT)/main


$(OUT)/main.o: $(OUT) $(wildcard src/*)
	$(ASM) -o $@ src/main.asm

$(OUT)/dict.o: $(OUT) $(wildcard dict/*)
	$(ASM) -o $@ dict/dict.asm

$(OUT)/lib.o: $(OUT) $(wildcard assignment-1-io-library/*)
	$(MAKE) -C assignment-1-io-library
	cp assignment-1-io-library/build/lib.o $@


$(OUT)/main: $(TARGETS) $(OUT)/lib.o
	$(LD) -o $@ $^


run: $(OUT)/main
	./$(OUT)/main

test: all
	$(PYTHON) -m pytest --junitxml=report.xml test/


$(OUT):
	mkdir -p $(OUT)

clean:
	rm -rf $(OUT)/*
