; Starts dictionary definition
; Pointer to the head of the dictionary linked list will be stored with label passed in parameter
; Creates context 'ctx_dict'.
; Nesting or creating dictionaries without properly finishing previous is prohibited.
%macro dictionary_begin 1
    %ifctx ctx_dict
        %fatal 'New dictionary started before previous ended. Use dictionary_end'
    %endif
    %ifntoken %1
        %fatal 'Dictionary head label '%1' is not a valid token'
    %endif
    %ifdef %1
        %fatal 'Another dictionary exists with same label'
    %endif

    %push ctx_dict

    %define %$dict_head 0
    %define %$dict_head_label %1
%endmacro



; Ends dictionary definition and allows to define another one not connected with current
%macro dictionary_end 0
    %ifnctx ctx_dict
        %fatal 'Dictionary end macro was used before dictionary_begin'
    %endif
    %if %$dict_head == 0
        %fatal 'Dictionary is empty'
    %endif

    %$dict_head_label: dq %$dict_head

    %pop ctx_dict
%endmacro



; Macro for defining dictionary entry
; colon(key: str, label: token)
; Creates linked list item.
; Uses dictionary_begin and dictionary_end for managing context.
; Item head will have global label from parameters
; Data structure:
; item:
;   dq ptr to previous item or 0 for end
;   dq ptr to label string
;   dq ptr to key string
;   db key string data as with nul-terminator
;   db user provided entry value definition (not created here)
%macro colon 2
    %ifnctx ctx_dict
        %fatal 'Found dictionary record before start macro called'
    %endif

    %ifnstr %1
        %fatal 'Dictionary item '%1' name is not a string'
    %endif
    %ifntoken %2
        %fatal 'Dictionary item '%1' label '%2' is not a valid token'
    %endif
    %ifdef %2
        %fatal 'Dictionary item '%1' label already used'
    %endif

    %2:
        .next:          dq %$dict_head
        .label:         dq .label_data
        .value:         dq .value_data
        .label_data:    db %1, 0
        .value_data:

    %define %$dict_head %2
%endmacro