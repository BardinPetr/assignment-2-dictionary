%include "assignment-1-io-library/lib.inc"

%define ITEM_NEXT_POS    0
%define ITEM_LABEL_POS   8
%define ITEM_VALUE_POS   2*8


; long* find_word(char* key, long* dict)
; Search item by key in dictionary.
; Dictionary format: linked list
; item:
;   - dq ptr_next_item
;   - dq ptr_label
;   - dq ptr_value
;
; @param[rdi]   key     string item key to search for
; @param[rsi]   dict    dictionary first item pointer
; @return               pointer to entry or 0 if not found
global dict_find_word
dict_find_word:
    enter 2*8, 0

    mov [rbp-8], rdi
    mov [rbp-16], rsi

.loop:
    mov rdi, [rbp-16]
    call dict_get_key

    mov rdi, rax
    mov rsi, [rbp-8]
    call string_equals
    test rax, rax
    jnz .found

    mov rdi, [rbp-16]
    mov rdi, [rdi+ITEM_NEXT_POS]

    test rdi, rdi
    jz .not_found

    mov [rbp-16], rdi
    jmp .loop


.found:
    mov rax, [rbp-16]
    jmp .end

.not_found:
    xor rax, rax

.end:
    leave
    ret


; char* dict_get_key(long* ptr)
; Return key string of dictionary item
; @param[rdi]   ptr     pointer to dict item structure
; @return               pointer to string
global dict_get_key
dict_get_key:
    mov rax, [rdi + ITEM_LABEL_POS]
    ret

; char* dict_get_value(long* ptr)
; Return value string of dictionary item
; @param[rdi]   ptr     pointer to dict item structure
; @return               pointer to string
global dict_get_value
dict_get_value:
    mov rax, [rdi+ITEM_VALUE_POS]
    ret
