import random
import unittest
from contextlib import contextmanager
from itertools import chain

from test.impl.app_test_suite import AppTestSuite
from test.lib.utils import random_string, quote, random_dicts

TEST_END_STRING = "#'test_end"
TEST_NOT_FOUND_STRING = "#'not_found"


class DictionaryTest(AppTestSuite):

    @staticmethod
    def _prepare_template_params_from_dict(dicts: dict[str, dict[str, str]],
                                           tests: list[tuple[str, str]],
                                           add_quotes=True) -> [dict, dict]:
        # labels for test strings
        string_labels = {i: f"label_{random_string()}" for i, _ in tests}
        strings = [
            {
                "label": label,
                "value": quote(value, add_quotes)
            }
            for value, label in string_labels.items()
        ]

        # reverse lookup for dictionary item labels (second param of colon)
        dict_item_labels = {
            dict_name: {key: f"label_{random_string()}" for key in dict_value.keys()}
            for dict_name, dict_value in dicts.items()
        }

        # convert native dicts to template params
        dicts = [
            {
                "label": dict_name,
                "items": [
                    {
                        "key": quote(k, add_quotes),
                        "value": quote(v, add_quotes),
                        "label": dict_item_labels[dict_name][k]
                    }
                    for k, v in dict_content.items()
                ]
            }
            for dict_name, dict_content in dicts.items()
        ]

        tests = [
            {
                "str": string_labels[string],
                "dict": dict_name
            }
            for string, dict_name in tests
        ]

        return {
            "strings": strings,
            "dicts": dicts,
            "tests": tests,
            "test_end": TEST_END_STRING,
            "not_found": TEST_NOT_FOUND_STRING
        }, dict_item_labels

    def _parse_test_output(self, stdout):
        # Split stdout by test end markers, cleanup and replace text not found entries with Nones
        test_results = [
            None if (line := i.strip()) == TEST_NOT_FOUND_STRING else line
            for i in stdout.split(TEST_END_STRING)[:-1]
        ]
        return test_results

    @contextmanager
    def _nasm_compile_template(self, template_path: str, template_params) -> str:
        with self.compile_mustache(template_path, template_params) as asm_file, \
                self.nasm_compile(asm_file) as build_file, \
                self.link([build_file, 'build/dict.o', 'build/lib.o']) as res:
            yield res

    def _check_responses(self, dicts: dict[str, dict[str, str]], tests: list[tuple[str, str]], results: list[str]):
        for (key, dict_name), res in zip(tests, results):
            original = dicts[dict_name].get(key, None)
            if original is None:
                msg = "Found value for nonexistent key"
            else:
                msg = "Not found value for existing key" if res is None else "Found invalid value"
            self.assertEqual(original, res, msg)

    def _do_tests(self, dicts, tests):
        random.shuffle(tests)

        params, dict_item_labels = self._prepare_template_params_from_dict(dicts, tests)

        with self._nasm_compile_template("test/test_dict.nasm.mustache", params) as target:
            _, stdout, _ = self.execute_assert_ok(target)

        test_results = self._parse_test_output(stdout)
        self._check_responses(dicts, tests, test_results)

    def test_dictionary_elf_representation(self):
        dicts = random_dicts(dict_size=10, dict_count=5)
        params, all_dict_labels = self._prepare_template_params_from_dict(dicts, [])

        with self._nasm_compile_template("test/test_dict.nasm.mustache", params) as target:
            for check_dict_name, check_dict in dicts.items():
                check_dict_labels = all_dict_labels[check_dict_name]
                real_dict, real_dict_labels = self._extract_dict(dict_label=check_dict_name, target=target)

                self.assertDictEqual(
                    check_dict, real_dict,
                    "Dictionary stored in resulting ELF file is not the same as defined in source file"
                )
                self.assertDictEqual(
                    check_dict_labels, real_dict_labels,
                    "Item labels stored in resulting ELF file is not the same as defined in source file"
                )

    def test_single_existing(self):
        dicts = random_dicts(dict_size=10, dict_count=1)
        dict_name = list(dicts.keys())[0]
        self._do_tests(
            dicts,
            [
                (i, dict_name)
                for i in dicts[dict_name].keys()
            ]
        )

    def test_single_nonexistent(self):
        dicts = random_dicts(dict_size=10, dict_count=1)
        dict_name = list(dicts.keys())[0]
        self._do_tests(
            dicts,
            [
                (key, dict_name)
                for _ in range(5)
                if (key := random_string(5, 10)) not in dicts[dict_name]
            ]
        )

    def test_multiple_own_keys(self):
        dicts = random_dicts(dict_size=5, dict_count=5)
        self._do_tests(
            dicts,
            [
                (key, dict_name)
                for dict_name, dict_value in dicts.items()
                for key, _ in dict_value.items()
            ]
        )

    def test_multiple_cross_any_keys(self):
        dicts = random_dicts(dict_size=5, dict_count=5)
        keys = list(chain.from_iterable([content.keys() for content in dicts.values()]))
        self._do_tests(
            dicts,
            [
                (key, dict_name)
                for dict_name, _ in dicts.items()
                for key in keys
            ] +
            [
                (key, dict_name)
                for dict_name, _ in dicts.items()
                for _ in range(5)
                if (key := random_string(5, 10)) not in keys
            ]
        )


if __name__ == '__main__':
    unittest.main()
