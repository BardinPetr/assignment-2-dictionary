from contextlib import contextmanager

from chevron import render

from test.lib.base_test_suite import BaseTestSuite
from test.lib.utils import create_tmp_file


class NasmTestSuite(BaseTestSuite):
    NASM_EXECUTABLE = "nasm"
    LD_EXECUTABLE = "ld"
    NASM_COMPILE_PARAMS = ['-g', '-felf64']

    def nasm_preprocess_file(self, path: str) -> str:
        _, stdout, _ = self.execute_assert_ok([
            self.NASM_EXECUTABLE, "-E", path
        ])
        return stdout

    @contextmanager
    def nasm_compile(self, in_file: str) -> str:
        with create_tmp_file() as out_file:
            code, _, _ = self.execute_assert_ok([
                self.NASM_EXECUTABLE, *self.NASM_COMPILE_PARAMS, "-o", out_file, in_file
            ])
            yield out_file

    @contextmanager
    def link(self, file_paths: list[str]) -> str:
        with create_tmp_file() as out_file:
            self.execute_assert_ok([
                self.LD_EXECUTABLE, "-o", out_file, *file_paths
            ])
            yield out_file

    @contextmanager
    def compile_mustache(self, path, data) -> str:
        with open(path, 'r') as f:
            try:
                res = render(f, data)
            except Exception as ex:
                self.fail(f"Tests are invalid: rendering asm template failed: {ex}")

        with create_tmp_file(res) as outfile:
            yield outfile
