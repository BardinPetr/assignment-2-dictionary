import os.path
import unittest
from subprocess import Popen, PIPE
from typing import Optional


class BaseTestSuite(unittest.TestCase):

    def __init__(self, *args, timeout: Optional[int] = None, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.timeout = 1 if timeout is None else timeout
        self.target_cwd = os.getcwd().replace('/test', '')

    def make(self, args: Optional[list[str]] = None):
        if args is None:
            args = ["all"]
        self.execute_assert_ok(["make", *args])

    def execute(self, path: str | list[str], stdin: str = "") -> tuple[int, str, str]:
        with Popen(path, shell=False, stdin=PIPE, stdout=PIPE, stderr=PIPE, text=True, cwd=self.target_cwd) as p:
            try:
                out = p.communicate(stdin, self.timeout)
            except TimeoutError:
                self.fail("Execution timeout reached")
            except Exception as ex:
                self.fail(f"Execution ended with exception: {ex}")

            stdout, stderr = map(
                lambda x: "" if x is None else x.strip(),
                out
            )
            code = p.returncode

            self.assertNotEqual(code, -11, 'Segmentation fault')

        return code, stdout, stderr

    def execute_assert_ok(self, path: str | list[str]):
        code, stdout, stderr = self.execute(path)
        self.assertEqual(0, code, stderr)
        return code, stdout, stderr
