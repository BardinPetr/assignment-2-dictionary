import lief

STRING_LIMIT = 1000


class ELFExplorer:

    def __init__(self, binary_path: str):
        self._elf = lief.parse(binary_path)
        self._sym = {i.name: i.value for i in self._elf.symbols}
        self._sym_rev = {addr: lbl for lbl, addr in self._sym.items()}

    def label(self, ptr: int) -> tuple[int, str]:
        return ptr, self._sym_rev.get(ptr, None)

    def unpack(self,
               base: int | str,
               offset: int = 0,
               sz_byte: int = 8) -> tuple[int, str]:
        if isinstance(base, str):
            base = self._sym[base]
        data = self._elf.get_content_from_virtual_address(base + offset, sz_byte)
        data = int.from_bytes(data, byteorder="little")
        return self.label(data)

    def load_string(self, ptr: int) -> str:
        res = ""
        for i in range(STRING_LIMIT):
            if not (char := self._elf.get_content_from_virtual_address(ptr + i, 1)[0]):
                break
            res += chr(char)
        else:
            raise Exception(f"String at {self.label(ptr)} is invalid")
        return res


a = ELFExplorer("./build/main")
