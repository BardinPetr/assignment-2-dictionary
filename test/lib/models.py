import logging
from dataclasses import dataclass
from enum import IntEnum
from typing import Optional, Generic, TypeVar
from unittest import TestCase


class ErrorResponseCode(IntEnum):
    pass


ERC = TypeVar('ERC', bound=ErrorResponseCode)


@dataclass
class TestData(Generic[ERC]):
    stdin: str
    returns: ERC
    stdout_message: Optional[str] = None
    stderr_message: Optional[str] = None

    def do_assert(self, testcase: TestCase, code: int, stdout: str, stderr: str):
        logging.debug(f"Checking test case {self}: got {code=}, {stdout=}, {stderr=}")
        testcase.assertEqual(code, self.returns.value, f"Code invalid for test {self}")
        if self.stdout_message is not None:
            testcase.assertEqual(stdout, self.stdout_message, f"Invalid STDOUT message for test {self}")
        if self.stderr_message is not None:
            testcase.assertEqual(stderr, self.stderr_message, f"Invalid STDERR message for test {self}")
