import os
import re
from contextlib import contextmanager
from random import choices, randint
from string import digits, ascii_letters, punctuation
from tempfile import NamedTemporaryFile
from typing import Optional

ALPHABET = ascii_letters + digits
ALPHABET_FULL = re.sub('[\'\"]', "", ALPHABET + punctuation)
RANDOM_STRING_PREFIX = 'rnds'


def quote(x: str, add_quotes: bool) -> str:
    return f"'{x}'" if add_quotes else x


def random_string(min_len=10, max_len=30, full=False) -> str:
    min_len -= len(RANDOM_STRING_PREFIX)
    max_len -= len(RANDOM_STRING_PREFIX)
    max_len = max(min_len, max_len)
    if max_len < 1:
        raise ValueError("Invalid ranges")
    return RANDOM_STRING_PREFIX + ''.join(choices(ALPHABET_FULL if full else ALPHABET, k=randint(min_len, max_len)))


def random_dict(count=20) -> dict[str, str]:
    return {random_string(): random_string() for _ in range(count)}


def random_dicts(dict_count=10, dict_size=10) -> dict[str, dict[str, str]]:
    return {random_string(): random_dict(dict_size) for _ in range(dict_count)}


@contextmanager
def create_tmp_file(data: Optional[str] = None):
    file = NamedTemporaryFile("w", delete=False)
    path = file.name

    if data is not None:
        file.write(data)
        file.flush()
    file.close()

    try:
        yield path
    finally:
        try:
            os.remove(path)
        except FileNotFoundError:
            pass
