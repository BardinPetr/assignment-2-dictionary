from dataclasses import dataclass

from test.impl.elf_dict import ELFDictExplorer
from test.lib.models import ErrorResponseCode, TestData
from test.lib.nasm_test_suite import NasmTestSuite


class AppTestSuite(NasmTestSuite):
    TARGET = './build/main'

    def setUp(self) -> None:
        super().setUp()
        self.make()

    def _extract_dict(self, dict_label: str, target: str = None) -> tuple[dict[str, str], dict[str, str]]:
        elf = ELFDictExplorer(self.TARGET if target is None else target)
        try:
            src_dict = elf.unpack_dict(dict_label)
        except Exception as ex:
            self.fail(f"Failed to extract dictionary labeled '{dict_label}' from binary: {ex}")

        return {k: v for k, (_, v) in src_dict.items()}, \
            {k: lbl for k, (lbl, _) in src_dict.items()}


class AppErrorResponseCode(ErrorResponseCode):
    OK = 0
    NOT_FOUND = 1
    ERR_INPUT = 2


@dataclass
class AppTestData(TestData[AppErrorResponseCode]):
    def __post_init__(self):
        match self.returns:
            case AppErrorResponseCode.OK:
                self.stderr_message = ""
            case AppErrorResponseCode.NOT_FOUND:
                self.stdout_message = ""
                self.stderr_message = "Key not found"
            case AppErrorResponseCode.ERR_INPUT:
                self.stdout_message = ""
                self.stderr_message = "Input too long"
