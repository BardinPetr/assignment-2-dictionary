from test.lib.elf import ELFExplorer

DICT_LIMIT = 1000


class ELFDictExplorer(ELFExplorer):
    def unpack_dict_item(self, ptr: int) -> tuple[int, str, str]:
        next_ptr = self.unpack(ptr, 0)
        key_ptr = self.unpack(ptr, 8)
        value_ptr = self.unpack(ptr, 2 * 8)

        return (
            next_ptr[0],
            self.load_string(key_ptr[0]),
            self.load_string(value_ptr[0])
        )

    def unpack_dict(self, head_label: str) -> dict[str, tuple[str, str]]:
        next_ptr, _ = self.unpack(head_label)
        res = {}
        for _ in range(DICT_LIMIT):
            _, cur_label = self.label(next_ptr)
            cur_label = cur_label.split('.')[0]

            next_ptr, k, v = self.unpack_dict_item(next_ptr)
            res[k] = (cur_label, v)

            if next_ptr == 0:
                break
        else:
            raise Exception(f"Dict {head_label} is invalid")

        return res
