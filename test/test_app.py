import unittest

from test.impl.app_test_suite import AppTestSuite, AppTestData, AppErrorResponseCode
from test.lib.utils import random_string


class AppTest(AppTestSuite):
    DICT_NAME = 'main_dict'

    def _run_checked(self, tests: list[AppTestData]) -> None:
        for test in tests:
            code, stdout, stderr = self.execute([self.TARGET], test.stdin)
            test.do_assert(self, code, stdout, stderr)

    def _extract_cur_dict(self) -> tuple[dict[str, str], dict[str, str]]:
        return self._extract_dict(self.DICT_NAME)

    def test_existing(self):
        dict_values, _ = self._extract_cur_dict()
        tests = [
            AppTestData(stdin=k, returns=AppErrorResponseCode.OK, stdout_message=v)
            for k, v in dict_values.items()
        ]
        self._run_checked(tests)

    def test_nonexistent(self):
        dict_values, _ = self._extract_cur_dict()
        tests = [
            AppTestData(stdin=key, returns=AppErrorResponseCode.NOT_FOUND)
            for _ in range(10)
            if (key := random_string()) not in dict_values
        ]
        self._run_checked(tests)

    def test_invalid_input(self):
        tests = [
            AppTestData(stdin="", returns=AppErrorResponseCode.NOT_FOUND),
            AppTestData(stdin=random_string(min_len=255, max_len=255), returns=AppErrorResponseCode.NOT_FOUND),
            AppTestData(stdin=random_string(min_len=256, max_len=256), returns=AppErrorResponseCode.ERR_INPUT)
        ]
        self._run_checked(tests)


if __name__ == '__main__':
    unittest.main()
