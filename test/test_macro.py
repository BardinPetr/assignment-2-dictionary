import unittest

from test.impl.app_test_suite import AppTestSuite
from test.lib.utils import create_tmp_file


class MacroTest(AppTestSuite):
    HEADER = """
        %include "dict/dict.inc"
        %include "dict/colon.inc"
        segment .data
    """

    def _compile_assert(self, code: str):
        code = self.HEADER + code
        with create_tmp_file(code) as src:
            with self.nasm_compile(src):
                pass

    def _compile_assert_raises(self, code: str):
        self.assertRaises(
            AssertionError,
            lambda: self._compile_assert(code)
        )

    def test_ok(self):
        self._compile_assert("""
            dictionary_begin dict
            colon 'test1', label1
            db 'test', 0
            colon 'test2', label2
            db 'test', 0
            dictionary_end
        """)

    def test_invalid_key(self):
        keys = ["asdasd", "label", "423242", "234sda", ""]

        for i in keys:
            self._compile_assert_raises(f"""
                dictionary_begin dict
                colon {i}, label
                db 'test', 0
                dictionary_end
            """)

    def test_invalid_label(self):
        keys = ["'asdas'", "234asd", "42", "d^as", "+123"]

        for i in keys:
            self._compile_assert_raises(f"""
                dictionary_begin dict
                colon 'test', {i}
                db 'test', 0
                dictionary_end
            """)
            self._compile_assert_raises(f"""
                dictionary_begin {i}
                colon 'test', lbl
                db 'test', 0
                dictionary_end
            """)

    def test_used_label(self):
        lbl = 'test_label1'
        self._compile_assert_raises(f"""
            dictionary_begin dict
            colon 'test1', {lbl}
            db 'test1', 0
            colon 'test2', {lbl}
            db 'test2', 0
            dictionary_end
        """)

    def test_no_initialization(self):
        self._compile_assert_raises(f"""
            colon 'test1', test
            db 'test1', 0
        """)
        self._compile_assert_raises(f"""
            dictionary_begin dict
            colon 'test2', test2
            db 'test2', 0
            dictionary_end
            
            colon 'test1', test
            db 'test1', 0
        """)

    def test_empty_dict(self):
        self._compile_assert_raises(f"""
            dictionary_begin dict
            dictionary_end
        """)
        self._compile_assert_raises(f"""
            dictionary_end
            dictionary_begin dict
        """)
        self._compile_assert_raises(f"""
            dictionary_end
        """)

    def test_nested_dicts(self):
        self._compile_assert_raises(f"""
            dictionary_begin dict
                colon 'test1', test1
                db 'test1', 0
                
                dictionary_begin dict2
                    colon 'test2', test2
                    db 'test2', 0
                dictionary_end            
            dictionary_end
        """)
        self._compile_assert_raises(f"""
            dictionary_begin dict
                colon 'test1', test1
                db 'test1', 0
            dictionary_end            
            dictionary_begin dict
                colon 'test2', test2
                db 'test2', 0
            dictionary_end
        """)
        self._compile_assert(f"""
            dictionary_begin dict
                colon 'test1', test1
                db 'test1', 0
            dictionary_end            
            dictionary_begin dict2
                colon 'test2', test2
                db 'test2', 0
            dictionary_end
        """)


if __name__ == '__main__':
    unittest.main()
